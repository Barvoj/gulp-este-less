var gulp = require('gulp');
var GulpEste = require('gulp-este');
var cond = require('gulp-cond');
var eventStream = require('event-stream');
var minifyCss = require('gulp-minify-css');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var less = require('gulp-less');
var pathOs = require('path');
var concat = require('gulp-concat');

module.exports = (function() {

    GulpEste.prototype.less = function(paths, dest) {
        var watchMode = !!this.changedFilePath;
        
        if (!Array.isArray(paths)) {
            paths = [paths];
        }

        if( typeof dest === 'string'  )
        {
            return gulp.src( paths ).pipe(plumber(function(error) {
                if (watchMode) {
                    return this.emit('end');
                }})).pipe(less()).pipe(concat( dest ))
                .pipe(cond(this.production, minifyCss())).pipe(gulp.dest('.'));
        }
        else
        {
            var streams = paths.map((function(_this) {
                return function(path) {
                    return gulp.src(path, {
                        base: '.'
                    }).pipe(plumber(function(error) {
                        if (watchMode) {
                            return this.emit('end');
                        }
                    })).pipe(less()).pipe(gulp.dest('.')).pipe(rename(function(path) {
                        var buildPath, cssPath;
                        cssPath = pathOs.normalize('/css');
                        buildPath = pathOs.normalize('/build');
                        path.dirname = path.dirname.replace(cssPath, buildPath);
                    })).pipe(cond(_this.production, minifyCss())).pipe(gulp.dest('.'));
                }
            })(this));

            return eventStream.merge.apply(eventStream, streams);
        }

    };
	
	GulpEste.prototype.css = function(paths, dest) {
        var watchMode = !!this.changedFilePath;

        if (!Array.isArray(paths)) {
            paths = [paths];
        }

        gulp.src( paths ).pipe(plumber(function(error) {
            if (watchMode) {
                return this.emit('end');
            }})).pipe(concat( dest ))
            .pipe(cond(this.production, minifyCss())).pipe(gulp.dest('.'));
    };

})();